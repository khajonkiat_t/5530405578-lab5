package tantiwetchayanon.khajonkiat.lab5;

/** 
 * Mr.Khajonkiat Tantiwetchayanon
 * ID 553040557-8 
 * Lab 5 Section 2  
 */

public class Player extends tantiwetchayanon.khajonkiat.lab4.Player { // class Player is subclass Player in package tantiwetchayanon.khajonkiat.lab4
	String language;

	public Player(String name, int bDay, int bMonth, int bYear, //constructor from superclass
			double weight, double height) {
		super(name, bDay, bMonth, bYear, weight, height);
		
	}

	public Player(String name, int bDay, int bMonth, int bYear, // constructor from superclass and add language = language to constructor
			double weight, double height, String language) {
		super(name, bDay, bMonth, bYear, weight, height);
		this.language = language;
		
	}
	public void speak() {
		System.out.print("Speak" + language); // ����ʴ���
	}
}
