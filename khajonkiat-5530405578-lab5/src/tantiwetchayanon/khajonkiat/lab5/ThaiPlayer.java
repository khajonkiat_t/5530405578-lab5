package tantiwetchayanon.khajonkiat.lab5;

/** 
 * Mr.Khajonkiat Tantiwetchayanon
 * ID 553040557-8 
 * Lab 5 Section 2  
 */

public class ThaiPlayer extends tantiwetchayanon.khajonkiat.lab5.Player { // class ThaiPlayer is subclass Player in package tantiwetchayanon.khajonkiat.lab5

	private String award;
	
	protected ThaiPlayer(String name, int bDay, int bMonth, int bYear, // constructor from superclass
			double weight, double height) {
		super(name, bDay, bMonth, bYear, weight, height);
	}

	protected ThaiPlayer(String name, int bDay, int bMonth, int bYear, // constructor from superclass and add award = award to constructor
			double weight, double height, String award) {
		super(name, bDay, bMonth, bYear, weight, height);
		this.award = award;
	}
	public void speak() {				// speak is method ����Ѻ�ʴ��Ӿٴ 
		System.out.println("�ٴ������"); 
	}
	public void setAward(String awardInput){ // ��õ�駤�� Award
		this.award = awardInput;
		
	}
	public String getAward() { // �����ҹ��� Award
		return award;
	}

}
